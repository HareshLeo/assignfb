package testCases;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import base.Base;
import pages.FBPage;
import utilities.configReader;
import static base.Base.driver;
import java.io.IOException;

public class FBLogin extends configReader {
	@BeforeSuite
	public void initBrowser() {
		Base.initBrowser(configReader.url);
	}

	@AfterSuite
	public void closeBrowser() throws IOException {
		Base.closeBrowser();

	}

	@Test
	public void login() {

		FBPage fb = new FBPage(driver);
		Base.setValue(FBPage.emailID, configReader.usrName);
		Base.setValue(FBPage.pswd, configReader.pswd);
		Base.click(FBPage.loginBtn);

	}

}
