package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FBPage {

	@FindBy(name = "email")
	public static WebElement emailID;

	@FindBy(id = "pass")
	public static WebElement pswd;

	@FindBy(css = "input[value='Log In']")
	public static WebElement loginBtn;

	public FBPage(WebDriver driver) {
		PageFactory.initElements(driver, this);

	}

}
