package base;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Base {
	public static String userDir = System.getProperty("user.dir");
	public static WebDriver driver = null;
	static WebDriverWait wait;

	public static void initBrowser(String url) {

		String path = userDir + "\\src\\main\\resources\\drivers\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", path);

		driver = new ChromeDriver();
		wait = new WebDriverWait(driver, 15);
		driver.get(url);
	}

	public static void waitForElementToBeClickable(WebElement element) {
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public static void setValue(WebElement ele, String value) {
		waitForElementToBeClickable(ele);
		ele.sendKeys(value);
	}

	public static void click(WebElement ele) {
		waitForElementToBeClickable(ele);
		ele.click();
	}

	public static void closeBrowser() throws IOException {
		driver.quit();
		Runtime rt = Runtime.getRuntime();
		rt.exec("taskkill /f /t /im chromedriver.exe");

	}

}
