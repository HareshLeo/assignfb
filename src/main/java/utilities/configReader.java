package utilities;

import java.io.FileInputStream;
import java.util.Properties;

import base.Base;

public class configReader {

	Properties p = new Properties();
	String path = Base.userDir + "\\src\\main\\resources\\config\\testData.properties";
	public static String url;
	public static String usrName;
	public static String pswd;

	public configReader() {
		try {

			FileInputStream fis = new FileInputStream(path);
			p.load(fis);
			url = p.getProperty("url");
			usrName = p.getProperty("userName");
			pswd = p.getProperty("password");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
